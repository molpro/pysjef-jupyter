FROM jupyter/minimal-notebook

RUN conda config --add channels conda-forge
RUN conda install 'python>=3.9' 'numpy>=1.17' 'boost-cpp>=1.71' pugixml=1.10 'cython>=0.24' 'lxml>=4.0' pip regex c-compiler cxx-compiler rsync matplotlib cmake make scipy
ENV CONDA_DIR /opt/conda
ENV CONDA_PREFIX ${CONDA_DIR}
ENV PATH ${CONDA_PREFIX}/bin:$PATH
ENV _CONDA_PYTHON_SYSCONFIGDATA_NAME _sysconfigdata_x86_64_conda_cos6_linux_gnu

RUN mkdir -p ~/.ssh && chmod go-rwx ~/.ssh && echo 'StrictHostKeyChecking accept-new' > ~/.ssh/config

RUN mkdir -p ~/.src

RUN cd ~/.src && git clone https://gitlab.com/molpro/pysjef && cd pysjef && PREFIX=${CONDA_PREFIX} bash build.sh 

RUN cd ~/.src && git clone https://gitlab.com/molpro/pysjef_molpro && cd pysjef_molpro && bash build.sh

RUN cd ~/.src && git clone https://gitlab.com/molpro/sjef.git && mkdir -p sjef/cmake-build && cd sjef/cmake-build && cmake .. && cmake --build .
USER root
RUN cd ~/.src/sjef/cmake-build && cmake --build . --target install && echo '#!/bin/sh' > /usr/local/bin/cmolpro && echo '/usr/local/bin/sjef -s molpro $*' >> /usr/local/bin/cmolpro && chmod ugo+x /usr/local/bin/cmolpro
USER jovyan

#RUN conda install other-python-packages
