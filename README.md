## Jupyter with pysjef
This container provides Jupyter notebook service accessed via a web browser.  Support is provided for [pysjef](https://molpro.gitlab.io/pysjef), including Molpro extensions, including the ability to interact with remote backends via ssh.  Some commonly-used Python packages are also bundled, including [Matplotlib](https://matplotlib.org) and [SciPy](https://scipy.org).
### Use
From the directory containing your work, `docker-compose --project-directory <this-directory> up --build`
Then point a web brower to [https://localhost:8888] and enter the token revealed by the output from `docker-compose`.  The current directory (or configurable alternative) is available in the Jupyter server as `work/`.
### Customisation
Via the `.env` file,
 
- `SSH_IDENTITY_FILE` should specify the file name of a private ssh key that does not need a password, and which can access any hosts to be used as sjef backends.
- `PORT` gives the port number of the host on which web service will be provided.
- `PWD` specifies the host directory to be mounted as ~/work in the container. In linux and macos,this can be omitted, giving the current directory when the container is launched. On Windows, it has to be set explicitly.

If additional Python packages are wanted, they can be specified at the end of `Dockerfile`